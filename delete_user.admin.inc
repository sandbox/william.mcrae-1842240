<?php

/**
 * @file
 * Administration functions for delete user module.
 */

/**
 * Creates an FAPI array for the module settings.
 *
 * @return array
 *   FAPI array for the module settings.
 */
function delete_user_settings() {

  $form['purge'] = array(
    '#type' => 'fieldset',
    '#title' => t('Registration'),
  );

  $purge_options = array(
    0 => t('Never delete'),
    86400 => t('1 Day'),
    172800 => t('2 Days'),
    259200 => t('3 Days'),
    345600 => t('4 Days'),
    432000 => t('5 Days'),
    518400 => t('6 Days'),
    604800 => t('1 Week'),
    1209600 => t('2 Weeks'),
    2592000 => t('1 Month'),
    7776000 => t('3 Months'),
    15379200 => t('6 Months'),
    30758400 => t('1 Year'),
  );

  $form['purge']['delete_user_purge_never_user_interval'] = array(
    '#type' => 'select',
    '#title' => t('Delete users who have never authenticated after'),
    '#options' => $purge_options,
    '#default_value' => variable_get('delete_user_purge_never_user_interval', 0),
    '#description' => t("If enabled user who have never authenticated will be removed the given time after the account was created."),
  );

  return system_settings_form($form);
}
